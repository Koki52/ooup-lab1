#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char const *dogGreet(void) {
    return "vau!";
}

char const *dogMenu(void) {
    return "kuhanu govedinu";
}

char const *catGreet(void) {
    return "mijau!";
}

char const *catMenu(void) {
    return "konzerviranu tunjevinu";
}

typedef struct Animal {
    char *name;

    const char *(**fun_table)(void);
} Animal;

char const *(*dog_fun_table[2])(void) = {dogGreet, dogMenu};

char const *(*cat_fun_table[2])(void) = {catGreet, catMenu};

Animal initAnimal(char *name, const char *(**fun_table)(void), Animal *animal) {
    animal->name = name;
    animal->fun_table = fun_table;
    return *animal;
}

// vracas pointer na memoriju koja postaje slobodna izlaskom iz funkcije
//Animal createDogStack(char *name) {
//    Animal dog;
//    return initAnimal(name, dog_fun_table, &dog);
//}

// ocekivana implementacija, a la konstruktor
// brine se za konstrukciju objekta, ne i alokaciju memorije
void constructDog(Animal* animal, char* name) {
    initAnimal(name, dog_fun_table, animal);
}

Animal constructCat(Animal* animal, char *name) {
    initAnimal(name, cat_fun_table, animal);
}

Animal *createDog(char *name) {
    Animal *dog = (Animal *) malloc(sizeof(Animal));
    constructDog(dog, name);
    return dog;
}

Animal *createCat(char *name) {
    Animal *cat = (Animal *) malloc(sizeof(Animal));
    constructCat(cat, name);
    return cat;
}

void animalPrintGreeting(Animal *animal) {
    printf("%s\n", (char *) animal->fun_table[0]());
}

void animalPrintMenu(Animal *animal) {
    printf("%s\n", animal->fun_table[1]());
}

// Why doesn't this work???
Animal **createMultipleDogs(int count) {
    int i;
    Animal **dogs = (Animal **) malloc(sizeof(Animal *) * count);

    for (i = 0; i < count; i++) {
        // ova alokacija stvara probleme jer je na stogu
        // cim izadjes iz funkcije, ta memorija postaje slobodna
        // char dog_name[6];
        char* dog_name = malloc(6 * sizeof(char));
        dog_name[0] = 0; // trik, instant 0 length string
        char dog_id[2]; // only for this example, this has to be calculated

        sprintf(dog_id, "%d", i);
        strcat(dog_name, "Dog_");
        strcat(dog_name, dog_id);
        dogs[i] = createDog(dog_name); // Zasto se ovo polomi pobogu???
    }

    return dogs;
}

void freeAnimals(Animal **dogs, int num_of_dogs) {
    int i;
    for (i = 0; i < num_of_dogs; i++) {
        free(dogs[i]);
    }
    free(dogs);
}


void testAnimals(void) {
    int i;

    Animal *p1 = createDog("Hamlet");
    Animal *p2 = createCat("Ofelija");
    Animal *p3 = createDog("Polonije");

    animalPrintGreeting(p1);
    animalPrintGreeting(p2);
    animalPrintGreeting(p3);

    animalPrintMenu(p1);
    animalPrintMenu(p2);
    animalPrintMenu(p3);

    free(p1);
    free(p2);
    free(p3);

    Animal **dogs = createMultipleDogs(10);
    for (i = 0; i < 10; i++) {
        printf("%s\n", dogs[i]->name);
    }

    freeAnimals(dogs, 10);
}

int main(int argc, char *argv[]) {
    testAnimals();
    Animal hamlet;
    constructDog(&hamlet, "Hamlet");
    Animal polonije;
    constructDog(&polonije, "Polonije");
    Animal ofelija;
    constructCat(&ofelija, "Ofelija");

    printf("%s\n", hamlet.name);
    printf("%s\n", polonije.name);
    printf("%s\n", ofelija.name);

    return 0;
};
